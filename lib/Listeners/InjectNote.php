<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes\Listeners;

use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCP\AppFramework\Http\Events\BeforeLoginTemplateRenderedEvent;
use OCP\AppFramework\Services\IInitialState;
use OCP\DB\Exception;
use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\IAppConfig;
use OCP\IRequest;
use OCP\Util;

/**
 * @template-implements IEventListener<BeforeLoginTemplateRenderedEvent>
 */
class InjectNote implements IEventListener {
	public function __construct(
		private IRequest $request,
		private IAppConfig $appConfig,
		private IInitialState $initialState,
		private Manager $manager,
	) {
	}


	/**
	 * @throws Exception
	 * @throws \JsonException
	 */
	public function handle(Event $event): void {
		if (!$event instanceof BeforeLoginTemplateRenderedEvent) {
			return;
		}
		$url = $this->request->getRequestUri();
		$match = $this->matches($url);
		if ($match !== false) {
			$notes = array_values(array_filter($this->manager->getNotes(), static function (Note $note) use ($match) {
				$pages = ($note->getPagesEnabled() ?? '{"login":true}');
				/** @var array $pagesArray */
				$pagesArray = json_decode($pages, true, 512, JSON_THROW_ON_ERROR);
				return isset($pagesArray[$match]) && $pagesArray[$match] === true;
			}));
			$isCentered = $this->appConfig->getValueString(Application::APP_NAME, 'centered', 'no') === 'yes';
			$githubMarkdown = $this->appConfig->getValueString(Application::APP_NAME, 'github_markdown', 'no') === 'yes';
			$this->initialState->provideInitialState('centered', $isCentered);
			$this->initialState->provideInitialState('github_markdown', $githubMarkdown);
			$this->initialState->provideInitialState(
				'notes',
				$notes
			);
			Util::addStyle(Application::APP_NAME, 'login_notes-main');
			Util::addScript(Application::APP_NAME, 'login_notes-main');
		}
	}

	private function matches(string $url): bool|string {
		$matches2FA = $this->matches2FA($url);
		if (count($matches2FA) > 1) {
			return $matches2FA[1];
		}
		if ($this->matches2FAChallenge($url)) {
			return 'challenge';
		}
		if ($this->matchesSAML($url)) {
			return 'saml';
		}
		if ($this->matchesLogin($url)) {
			return 'login';
		}
		return false;
	}

	private function matchesLogin(string $url): bool {
		return preg_match('%/login(\?.+)?$%m', $url) === 1;
	}

	private function matchesSAML(string $url): bool {
		return preg_match('%/saml/selectUserBackEnd(\?.+)?$%m', $url) === 1;
	}

	private function matches2FAChallenge(string $url): bool {
		return preg_match('/\/login\/selectchallenge(\?.+)?$/', $url) === 1;
	}

	/**
	 * @param string $url
	 * @return string[]
	 * @psalm-return array<array-key, string>
	 */
	private function matches2FA(string $url): array {
		preg_match('/\/login\/challenge\/(.+)*(\?.+)?$/', $url, $matches);
		return $matches;
	}
}
