<?php

/*
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

return ['routes' => [
	['name' => 'note#create', 'url' => '/notes', 'verb' => 'POST'],
	['name' => 'note#destroy', 'url' => '/notes/{id}', 'verb' => 'DELETE'],
	['name' => 'note#update', 'url' => '/notes/{id}', 'verb' => 'PATCH'],
	['name' => 'setting#set', 'url' => '/settings', 'verb' => 'POST']
]];
