<?php

declare(strict_types=1);

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes;

use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Model\NoteMapper;
use OCA\LoginNotes\Vendor\League\CommonMark\Environment\Environment;
use OCA\LoginNotes\Vendor\League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use OCA\LoginNotes\Vendor\League\CommonMark\Extension\ExternalLink\ExternalLinkExtension;
use OCA\LoginNotes\Vendor\League\CommonMark\Extension\GithubFlavoredMarkdownExtension;
use OCA\LoginNotes\Vendor\League\CommonMark\MarkdownConverter;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\DB\Exception;

class Manager {
	private MarkdownConverter $converter;

	public function __construct(
		private NoteMapper $noteMapper,
		private ITimeFactory $timeFactory,
	) {

		$config = [
			'external_link' => [
				'open_in_new_window' => true,
				'nofollow' => '',
				'noopener' => 'external',
				'noreferrer' => 'external',
			],
		];

		$environment = new Environment($config);
		$environment->addExtension(new CommonMarkCoreExtension());
		$environment->addExtension(new GithubFlavoredMarkdownExtension());
		$environment->addExtension(new ExternalLinkExtension());
		$this->converter = new MarkdownConverter($environment);
	}

	/**
	 * @throws DoesNotExistException
	 * @throws MultipleObjectsReturnedException|Exception
	 */
	public function getById(int $id): Note {
		return $this->noteMapper->getById($id);
	}

	/**
	 * @throws Exception
	 * @throws \JsonException
	 * @psalm-return Note
	 */
	public function create(string $text, array $pages): Note {
		$text = trim($text);
		$note = new Note();
		$note->setRawText($text);
		$note->setText($this->converter->convert($text)->getContent());
		$note->setCreatedAt($this->timeFactory->getTime());
		$note->setPagesEnabled(json_encode($pages, JSON_THROW_ON_ERROR));
		return $this->noteMapper->insert($note);
	}

	/**
	 * @throws DoesNotExistException
	 * @throws Exception
	 * @throws MultipleObjectsReturnedException
	 * @throws \JsonException
	 */
	public function update(int $noteId, string $text, array $pages): Note {
		$text = trim($text);
		$note = $this->noteMapper->getById($noteId);
		$note->setRawText($text);
		$note->setText($this->converter->convert($text)->getContent());
		$note->setPagesEnabled(json_encode($pages, JSON_THROW_ON_ERROR));
		$this->noteMapper->update($note);
		return $note;
	}

	/**
	 * @throws Exception
	 */
	public function delete(Note $note): void {
		$this->noteMapper->delete($note);
	}

	/**
	 * @throws Exception
	 */
	public function getNotes(?int $limit = null, ?int $offset = null): array {
		return $this->noteMapper->getNotes($limit, $offset);
	}
}
