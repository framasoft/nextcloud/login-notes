<!--
SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>

SPDX-License-Identifier: AGPL-3.0-only
-->

# 📝Admin notes

This app will show admin defined notes the log-in page. Markdown supported.
