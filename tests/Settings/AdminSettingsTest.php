<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCA\LoginNotes\Settings\AdminSettings;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\DB\Exception;
use OCP\IAppConfig;
use PHPUnit\Framework\MockObject\MockObject;

class AdminSettingsTest extends TestCase {
	private Manager|MockObject $manager;
	private IInitialState|MockObject $initialStateService;
	private IAppConfig|MockObject $appConfig;
	private AdminSettings $adminSettings;

	public function setUp(): void {
		parent::setUp();
		$this->manager = $this->createMock(Manager::class);
		$this->initialStateService = $this->createMock(IInitialState::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->adminSettings = new AdminSettings($this->manager, $this->initialStateService, $this->appConfig);
	}

	public function testGetPriority() : void {
		self::assertEquals(10, $this->adminSettings->getPriority());
	}

	public function testGetSection() : void {
		self::assertEquals('additional', $this->adminSettings->getSection());
	}

	public static function dataForTestGetForm(): array {
		return [
			[true, false, [new Note()]],
			[true, true, [new Note()]],
			[false, false, [new Note()]],
			[false, true, [new Note()]],
		];
	}

	/**
	 * @dataProvider dataForTestGetForm
	 * @param bool $centeredBool
	 * @param bool $github_markdown
	 * @param array $notes
	 * @throws Exception
	 */
	public function testGetForm(bool $centeredBool, bool $github_markdown, array $notes): void {
		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $centeredBool, $github_markdown): string {
			switch ($matcher->numberOfInvocations()) {
				case 1:
					$this->assertEquals('centered', $key);
					$this->assertEquals('no', $value);
					return $centeredBool ? 'yes' : 'no';
				case 2:
					$this->assertEquals('github_markdown', $key);
					$this->assertEquals('no', $value);
					return $github_markdown ? 'yes' : 'no';
			};
			return '';
		});

		$result = [
			['centered', $centeredBool],
			['github_markdown', $github_markdown],
			['notes', $notes],
			['pages', ['totp' => class_exists(\OCA\TwoFactorTOTP\AppInfo\Application::class), 'saml' => false, 'u2f' => false, 'email' => false, 'twofactor_nextcloud_notification' => false]]
		];

		$matcher = $this->exactly(4);
		$this->initialStateService->expects($matcher)->method('provideInitialState')->willReturnCallback(function ($key, $value) use ($matcher, $result): void {
			$count = $matcher->numberOfInvocations() - 1;
			$this->assertEquals($result[$count][0], $key);
			$this->assertEquals($result[$count][1], $value);
		});
		$this->manager->expects(self::once())->method('getNotes')->with()->willReturn($notes);
		$response = new TemplateResponse('login_notes', 'admin');
		self::assertEquals($response, $this->adminSettings->getForm());
	}
}
