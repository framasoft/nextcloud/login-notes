<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Controller;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Controller\SettingController;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IAppConfig;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;

class SettingsControllerTest extends TestCase {
	private SettingController $controller;
	private IAppConfig|MockObject $appConfig;

	public function setUp(): void {
		parent::setUp();

		$request = $this->createMock(IRequest::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->controller = new SettingController(Application::APP_NAME, $request, $this->appConfig);
	}

	public static function setAppValueDataProvider(): array {
		return [
			[null, null, []],
			[
				true,
				true,
				[
					['login_notes', 'centered', 'yes'],
					['login_notes', 'github_markdown', 'yes']
				]
			],
			[null, true, [
				['login_notes', 'github_markdown', 'yes']
			]],
			[false, null, [
				['login_notes', 'centered', 'no'],
			]],
			[false, true, [
				['login_notes', 'centered', 'no'],
				['login_notes', 'github_markdown', 'yes']
			]],
			[true, false, [
				['login_notes', 'centered', 'yes'],
				['login_notes', 'github_markdown', 'no']
			]],
			[false, false, [
				['login_notes', 'centered', 'no'],
				['login_notes', 'github_markdown', 'no']
			]]
		];
	}

	/**
	 * @dataProvider setAppValueDataProvider
	 * @param bool $centered
	 */
	public function testSetAppValue(?bool $centered, ?bool $github_markdown, array $params): void {
		$matcher = $this->exactly(count($params));
		$this->appConfig->expects($matcher)->method('setValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher, $params): bool {
			$count = $matcher->numberOfInvocations() - 1;
			$this->assertEquals($params[$count][0], $appId);
			$this->assertEquals($params[$count][1], $key);
			$this->assertEquals($params[$count][2], $value);
			return true;
		});

		$response = new DataResponse([], Http::STATUS_OK);
		self::assertEquals($response, $this->controller->set($centered, $github_markdown));
	}
}
