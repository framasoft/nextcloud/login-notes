<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Listeners;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\LoginNotes\Listeners\InjectNote;
use OCA\LoginNotes\Manager;
use OCP\AppFramework\Http\Events\BeforeLoginTemplateRenderedEvent;
use OCP\AppFramework\Services\IInitialState;
use OCP\EventDispatcher\Event;
use OCP\IAppConfig;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;

class InjectNoteTest extends TestCase {
	private IAppConfig|MockObject $appConfig;
	private IRequest|MockObject $request;
	private InjectNote $listener;
	private IInitialState|MockObject $initialState;
	private Manager|MockObject $manager;
	public const UID = 'someUID';
	private BeforeLoginTemplateRenderedEvent|MockObject $event;

	public function setUp(): void {
		parent::setUp();
		$this->request = $this->createMock(IRequest::class);
		$this->appConfig = $this->createMock(IAppConfig::class);
		$this->event = $this->createMock(BeforeLoginTemplateRenderedEvent::class);
		$this->initialState = $this->createMock(IInitialState::class);
		$this->manager = $this->createMock(Manager::class);

		$this->listener = new InjectNote($this->request, $this->appConfig, $this->initialState, $this->manager);
	}

	public function testHandleUnrelated(): void {
		$event = new Event();
		$this->request->expects($this->never())->method('getRequestUri');
		$this->listener->handle($event);
	}

	public function testHandleOtherPath(): void {
		$this->request->expects($this->once())->method('getRequestUri')->with()->willReturn('/someotherpath');
		$this->manager->expects($this->never())->method('getNotes')->with();
		$this->listener->handle($this->event);
	}

	public function testHandleLoginPath(): void {
		$this->request->expects($this->once())->method('getRequestUri')->with()->willReturn('/login');
		$this->manager->expects($this->once())->method('getNotes')->with()->willReturn([]);
		$matcher = $this->exactly(2);
		$this->appConfig->expects($matcher)->method('getValueString')->willReturnCallback(function ($appId, $key, $value) use ($matcher): string {
			switch ($matcher->numberOfInvocations()) {
				case 1:
					$this->assertEquals('centered', $key);
					$this->assertEquals('no', $value);
					break;
				case 2:
					$this->assertEquals('github_markdown', $key);
					$this->assertEquals('no', $value);
			};
			return 'no';
		});

		$matcher = $this->exactly(3);
		$this->initialState->expects($matcher)->method('provideInitialState')->willReturnCallback(function ($key, $value) use ($matcher): void {
			switch ($matcher->numberOfInvocations()) {
				case 1:
					$this->assertEquals('centered', $key);
					$this->assertFalse($value);
					break;
				case 2:
					$this->assertEquals('github_markdown', $key);
					$this->assertFalse($value);
					break;
				case 3:
					$this->assertEquals('notes', $key);
					$this->assertEquals([], $value);
			};
		});
		$this->listener->handle($this->event);
	}
}
