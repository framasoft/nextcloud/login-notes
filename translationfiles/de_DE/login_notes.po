# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Nextcloud package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Nextcloud 3.14159\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2023-10-16 11:54+0200\n"
"PO-Revision-Date: 2023-11-24 14:40+0000\n"
"Last-Translator: Ettore Atalan <atalanttore@googlemail.com>\n"
"Language-Team: German <https://weblate.framasoft.org/projects/nextcloud/"
"login-notes/de/>\n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2\n"

#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialAppInfoFakeDummyForL10nScript.php:2
msgid "Login notes"
msgstr "Login-Hinweise"

#. TRANSLATORS src/components/EditNote.vue:27
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:2
msgid "Enter the text for the updated login note here. Markdown supported."
msgstr ""
"Geben Sie hier den Text für den aktualisierten Login-Hinweis hier ein. "
"Markdown wird unterstützt."

#. TRANSLATORS src/components/EditNote.vue:30
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:4
msgid "Where to show this note?"
msgstr "Wo soll dieser Hinweis angezeigt werden?"

#. TRANSLATORS src/components/EditNote.vue:38
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:6
msgid "Show on login page"
msgstr "Auf der Anmeldeseite anzeigen"

#. TRANSLATORS src/components/EditNote.vue:39
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:8
msgid "The note will be shown on the login page"
msgstr "Der Hinweis wird auf der Anmeldeseite angezeigt"

#. TRANSLATORS src/components/EditNote.vue:49
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:10
msgid "Show on SAML login option selection"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:50
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:12
msgid ""
"The note will be shown on the page which asks the user which login option to "
"pick when SAML is used"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:60
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:14
msgid "Show on 2FA TOTP page"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:61
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:16
#, fuzzy
#| msgid "These notes will be shown on the login screen."
msgid ""
"The note will be shown on the 2FA TOTP page, when asking for the TOTP code"
msgstr "Diese Hinweise werden auf dem Anmeldebildschirm angezeigt."

#. TRANSLATORS src/components/EditNote.vue:71
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:18
msgid "Show on 2FA U2F page"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:72
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:20
msgid ""
"The note will be shown on the 2FA U2F page, when asking for the code from "
"the U2F device"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:82
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:22
msgid "Show on 2FA Nextcloud Notification page"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:83
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:24
msgid ""
"The note will be shown on the 2FA Nextcloud Notification page, when asking "
"to confirm the connection on another device"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:93
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:26
msgid "Show on 2FA challenge selection page"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:94
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:28
msgid ""
"The note will be shown on the page which asks the user which 2FA method to "
"use for the challenge"
msgstr ""

#. TRANSLATORS src/components/EditNote.vue:100
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:30
msgid "Update note"
msgstr "Login-Hinweis aktualisieren"

#. TRANSLATORS src/components/EditNote.vue:103
#. TRANSLATORS src/views/Settings.vue:87
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:32
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:72
#, fuzzy
#| msgid "Update note"
msgid "Create note"
msgstr "Login-Hinweis aktualisieren"

#. TRANSLATORS src/components/EditNote.vue:106
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:34
msgid "Cancel"
msgstr "Abbrechen"

#. TRANSLATORS src/components/ViewNote.vue:34
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:36
#, fuzzy
#| msgid "Update note"
msgid "Edit note"
msgstr "Login-Hinweis aktualisieren"

#. TRANSLATORS src/components/ViewNote.vue:43
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:38
msgid "Edit"
msgstr "Bearbeiten"

#. TRANSLATORS src/components/ViewNote.vue:46
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:40
msgid "Delete"
msgstr "Löschen"

#. TRANSLATORS src/views/Settings.vue:25
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:42
msgid "Notes on login screen"
msgstr "Hinweis auf dem Login-Bereich"

#. TRANSLATORS src/views/Settings.vue:27
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:44
msgid "Text alignment"
msgstr "Textausrichtung"

#. TRANSLATORS src/views/Settings.vue:32
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:46
msgid "Aligned"
msgstr "Linksbündig"

#. TRANSLATORS src/views/Settings.vue:34
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:48
msgid "Note content will be aligned to left (or right with RTL languages)."
msgstr ""
"Der Login-Hinweis wird linksbündig ausgerichtet (oder rechtsbündig bei RTL-"
"Sprachen)."

#. TRANSLATORS src/views/Settings.vue:39
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:50
msgid "Centered"
msgstr "Zentriert"

#. TRANSLATORS src/views/Settings.vue:41
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:52
msgid "Note content will be centered."
msgstr "Der Login-Hinweis wird zentriert ausgerichtet."

#. TRANSLATORS src/views/Settings.vue:43
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:54
msgid "Style"
msgstr ""

#. TRANSLATORS src/views/Settings.vue:45
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:56
msgid "Github Markdown Styles"
msgstr ""

#. TRANSLATORS src/views/Settings.vue:47
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:58
msgid "The note will be styled with Github Markdown Style."
msgstr ""

#. TRANSLATORS src/views/Settings.vue:49
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:60
msgid "Notes list"
msgstr "Liste der Login-Hinweise"

#. TRANSLATORS src/views/Settings.vue:51
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:62
msgid "These notes will be shown on the login screen."
msgstr "Diese Hinweise werden auf dem Anmeldebildschirm angezeigt."

#. TRANSLATORS src/views/Settings.vue:68
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:64
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:68
msgid "Add new note"
msgstr "Neuen Login-Hinweis anlegen"

#. TRANSLATORS src/views/Settings.vue:72
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:66
#: /var/home/tcit/dev/nextcloud/server/apps/login_notes/specialVueFakeDummyForL10nScript.js:70
msgid "No notes yet"
msgstr "Noch keine Notizen"

#~ msgid "This app will show admin defined notes the log-in page"
#~ msgstr ""
#~ "Diese App zeigt vom Administrator hinterlegte Hinweise auf dem Login-"
#~ "Bereich an."

#~ msgid "New login note"
#~ msgstr "Neuer Login-Hinweis"

#~ msgid "Enter the text for your new login note here. Markdown supported."
#~ msgstr ""
#~ "Geben Sie hier den Text für den neuen Login-Hinweis hier ein. Markdown "
#~ "wird unterstützt."
