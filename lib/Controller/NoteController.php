<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes\Controller;

use OCA\LoginNotes\Manager;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\DB\Exception;
use OCP\IRequest;

class NoteController extends Controller {

	public function __construct(
		string $AppName,
		IRequest $request,
		private Manager $manager,
	) {
		parent::__construct($AppName, $request);
	}

	/**
	 * @throws Exception|\JsonException
	 */
	public function create(string $text, array $pages): DataResponse {
		$note = $this->manager->create($text, $pages);
		return new DataResponse($note);
	}

	/**
	 * @throws \JsonException
	 */
	public function update(int $id, string $text, array $pages): DataResponse {
		try {
			$note = $this->manager->update($id, $text, $pages);
			return new DataResponse($note);
		} catch (DoesNotExistException $e) {
			return new DataResponse([], Http::STATUS_NOT_FOUND);
		} catch (MultipleObjectsReturnedException|Exception $e) {
			return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @return DataResponse
	 */
	public function destroy(int $id): DataResponse {
		try {
			$note = $this->manager->getById($id);
			$this->manager->delete($note);
			return new DataResponse($note);
		} catch (\Exception $e) {
			return new DataResponse([], Http::STATUS_NOT_FOUND);
		}
	}
}
