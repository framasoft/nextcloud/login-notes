<?php

declare(strict_types=1);

/**
 * Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes\Model;

use OCP\AppFramework\Db\Entity;

/**
 * Class Note
 *
 * @method void setText(string $text)
 * @method string getText()
 * @method void setRawText(string $rawText)
 * @method string getRawText()
 * @method void setCreatedAt(int $time)
 * @method int getCreatedAt()
 * @method void setPagesEnabled(string|null $pages)
 * @method string|null getPagesEnabled()
 *
 * @package OCA\LoginNotes\Model
 */
class Note extends Entity implements \JsonSerializable {
	public ?string $text = null;
	public ?string $rawText = null;
	public ?int $createdAt = null;
	public ?string $pagesEnabled = null;

	public function __construct() {
		$this->addType('rawText', 'string');
		$this->addType('text', 'string');
		$this->addType('createdAt', 'int');
		$this->addType('pagesEnabled', 'string');
	}

	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'text' => $this->text,
			'rawText' => $this->rawText,
			'createdAt' => $this->createdAt,
			'pagesEnabled' => json_decode(($this->pagesEnabled ?? '{"login":true}'), true, 512, JSON_THROW_ON_ERROR),
		];
	}
}
