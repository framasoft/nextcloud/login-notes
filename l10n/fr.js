OC.L10N.register(
    "login_notes",
    {
    "Login notes" : "Notes de connexion",
    "Enter the text for the updated login note here. Markdown supported." : "Entrez le texte pour la note mise à jour sur la page de connexion. Markdown supporté.",
    "Where to show this note?" : "Où afficher cette note ?",
    "Show on login page" : "Afficher sur la page de connexion",
    "The note will be shown on the login page" : "Cette note sera affichée sur la page de connexion",
    "Show on SAML login option selection" : "Afficher sur la page de sélection de connexion SAML",
    "The note will be shown on the page which asks the user which login option to pick when SAML is used" : "La note sera affichée sur la page demandant à l'utilisateur quelle option de connexion choisir lorsque SAML est utilisé",
    "Show on 2FA TOTP page" : "Afficher sur la page 2FA TOTP",
    "The note will be shown on the 2FA TOTP page, when asking for the TOTP code" : "Cette note sera affichée sur la page 2FA TOTP, lors de la demande du code TOTP",
    "Show on 2FA U2F page" : "Afficher sur la page 2FA U2F",
    "The note will be shown on the 2FA U2F page, when asking for the code from the U2F device" : "La note sera affichée sur la page 2FA U2F, lors de la demande du code à partir de l'appareil U2F",
    "Show on 2FA Nextcloud Notification page" : "Afficher sur la page de notification 2FA de Nextcloud",
    "The note will be shown on the 2FA Nextcloud Notification page, when asking to confirm the connection on another device" : "La note sera affichée sur la page de notification 2FA de Nextcloud, lors de la demande de confirmation de la connexion sur un autre appareil",
    "Show on 2FA challenge selection page" : "Afficher sur la page de sélection de la méthode 2FA",
    "The note will be shown on the page which asks the user which 2FA method to use for the challenge" : "La note sera affichée sur la page qui demande à l'utilisateur quelle méthode 2FA utiliser pour la connexion",
    "Update note" : "Mettre à jour la note",
    "Create note" : "Créer une note",
    "Cancel" : "Annuler",
    "Edit note" : "Modifier la note",
    "Edit" : "Modifier",
    "Delete" : "Supprimer",
    "Notes on login screen" : "Notes sur la page de connexion",
    "Text alignment" : "Alignement du texte",
    "Aligned" : "Aligné",
    "Note content will be aligned to left (or right with RTL languages)." : "Le contenu des notes sera aligné à gauche (ou à droite dans le cas de systèmes d'écriture sinistroverses).",
    "Centered" : "Centré",
    "Note content will be centered." : "Le contenu des notes sera centré.",
    "Style" : "Style",
    "Github Markdown Styles" : "Styles Markdown Github",
    "The note will be styled with Github Markdown Style." : "La note sera stylisée avec le style du Markdown GitHub.",
    "Notes list" : "Liste de notes",
    "These notes will be shown on the login screen." : "Ces notes seront affichées sur la page de connexion.",
    "Add new note" : "Ajouter une nouvelle note",
    "No notes yet" : "Pas encore de note"
},
"nplurals=2; plural=n > 1;");
