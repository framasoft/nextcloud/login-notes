/*
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { loadState } from '@nextcloud/initial-state'
import './style.scss'
import './markdown.css'

const notes = loadState('login_notes', 'notes', [])
const centered = loadState('login_notes', 'centered', false)
const githubMarkdown = loadState('login_notes', 'github_markdown', false)
// insert warning
const submit = document.querySelector('.wrapper main') || document.querySelector('body > .wrapper > div > div')
if (submit) {
	const noteWrapper = document.createElement('div')
	noteWrapper.classList.add('login-notes-wrapper')
	const fragment = document.createDocumentFragment()
	submit.insertAdjacentHTML('afterend', '<div class="login-notes-wrapper"></div>')
	notes.forEach((note) => {
		const warning = document.createElement('div')
		warning.classList.add('login-notes-rendered')
		if (githubMarkdown) {
			warning.classList.add('markdown-body')
		}
		if (centered) {
			warning.classList.add('centered')
		}
		warning.dir = 'auto'
		warning.innerHTML = note.text
		fragment.appendChild(warning)
	})
	noteWrapper.appendChild(fragment)
	submit.insertAdjacentElement('afterend', noteWrapper)
}
