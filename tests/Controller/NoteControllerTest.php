<?php

/**
 * SPDX-FileCopyrightText: 2021 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

declare(strict_types=1);

namespace OCA\LoginNotes\Tests\Controller;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Controller\NoteController;
use OCA\LoginNotes\Manager;
use OCA\LoginNotes\Model\Note;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IRequest;
use PHPUnit\Framework\MockObject\MockObject;

class NoteControllerTest extends TestCase {
	private NoteController $controller;
	/**
	 * @var Manager|MockObject
	 */
	private $manager;

	public function setUp(): void {
		parent::setUp();

		$request = $this->createMock(IRequest::class);
		$this->manager = $this->createMock(Manager::class);
		$this->controller = new NoteController(Application::APP_NAME, $request, $this->manager);
	}

	public function testCreateNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('create')->with('new note')->willReturn($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->create('new note', ['login' => true]));
	}

	public function testUpdateNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willReturn($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->update(1, 'updated note', ['login' => true, 'totp' => false]));
	}

	public function testUpdateNoteWithNotExistingNote(): void {
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willThrowException(new DoesNotExistException(''));
		$response = new DataResponse([], Http::STATUS_NOT_FOUND);
		self::assertEquals($response, $this->controller->update(1, 'updated note', ['login' => true, 'totp' => false]));
	}

	public function testUpdateNoteWithMultipleObjectReturned(): void {
		$this->manager->expects(self::once())->method('update')->with(1, 'updated note')->willThrowException(new MultipleObjectsReturnedException(''));
		$response = new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		self::assertEquals($response, $this->controller->update(1, 'updated note', ['login' => true, 'totp' => false]));
	}

	public function testDeleteNote(): void {
		$note = $this->createMock(Note::class);
		$this->manager->expects(self::once())->method('getById')->with(1)->willReturn($note);
		$this->manager->expects(self::once())->method('delete')->with($note);
		$response = new DataResponse($note);
		self::assertEquals($response, $this->controller->destroy(1));
	}

	public function testDeleteNoteWithNotExistingNote(): void {
		$this->manager->expects(self::once())->method('getById')->with(1)->willThrowException(new DoesNotExistException(''));
		$response = new DataResponse([], Http::STATUS_NOT_FOUND);
		self::assertEquals($response, $this->controller->destroy(1));
	}
}
