/*
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

module.exports = {
	extends: ['@nextcloud/stylelint-config'],
	ignoreFiles: ['src/markdown.css', 'src/**/*.js'],
}
