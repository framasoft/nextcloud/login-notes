<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes\Controller;

use OCA\LoginNotes\AppInfo\Application;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\IAppConfig;
use OCP\IRequest;

class SettingController extends Controller {

	public function __construct(
		string $AppName,
		IRequest $request,
		private IAppConfig $appConfig,
	) {
		parent::__construct($AppName, $request);
	}

	public function set(?bool $centered, ?bool $github_markdown): DataResponse {
		if ($centered !== null) {
			$this->appConfig->setValueString(Application::APP_NAME, 'centered', $centered ? 'yes' : 'no');
		}
		if ($github_markdown !== null) {
			$this->appConfig->setValueString(Application::APP_NAME, 'github_markdown', $github_markdown ? 'yes' : 'no');
		}
		return new DataResponse([], Http::STATUS_OK);
	}
}
