<?php

/**
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */


namespace OCA\LoginNotes\Settings;

use OCA\LoginNotes\AppInfo\Application;
use OCA\LoginNotes\Manager;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use OCP\DB\Exception;
use OCP\IAppConfig;
use OCP\Settings\ISettings;
use OCP\Util;

class AdminSettings implements ISettings {

	public function __construct(
		private Manager $manager,
		private IInitialState $initialState,
		private IAppConfig $appConfig,
	) {
	}

	/**
	 * @throws Exception
	 */
	public function getForm(): TemplateResponse {
		$isCentered = $this->appConfig->getValueString(Application::APP_NAME, 'centered', 'no') === 'yes';
		$githubMarkdown = $this->appConfig->getValueString(Application::APP_NAME, 'github_markdown', 'no') === 'yes';
		$twoFATotpEnabled = class_exists(\OCA\TwoFactorTOTP\AppInfo\Application::class);
		$samlEnabled = class_exists(\OCA\User_SAML\AppInfo\Application::class);
		$twoFAU2FEnabled = class_exists(\OCA\TwoFactorU2F\AppInfo\Application::class);
		$twoFAEmailEnabled = class_exists(\OCA\TwoFactorEmail\AppInfo\Application::class);
		$twoFANotificationEnabled = class_exists(\OCA\TwoFactorNextcloudNotification\AppInfo\Application::class);
		$this->initialState->provideInitialState('centered', $isCentered);
		$this->initialState->provideInitialState('github_markdown', $githubMarkdown);
		$this->initialState->provideInitialState('notes', $this->manager->getNotes());
		$this->initialState->provideInitialState('pages', ['totp' => $twoFATotpEnabled, 'saml' => $samlEnabled, 'u2f' => $twoFAU2FEnabled, 'email' => $twoFAEmailEnabled, 'twofactor_nextcloud_notification' => $twoFANotificationEnabled]);

		Util::addStyle(Application::APP_NAME, 'login_notes-settings');
		Util::addScript(Application::APP_NAME, 'login_notes-settings');

		return new TemplateResponse('login_notes', 'admin');
	}

	public function getSection(): string {
		return 'additional';
	}

	public function getPriority(): int {
		return 10;
	}
}
