<?php

declare(strict_types=1);

/**
 * Copyright (c) 2020 Thomas Citharel <nextcloud@tcit.fr>
 * SPDX-FileCopyrightText: 2020 Framasoft <https://framasoft.org>
 * SPDX-FileContributor: Thomas Citharel <thomas.citharel@framasoft.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

namespace OCA\LoginNotes\Model;

use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;
use OCP\AppFramework\Db\QBMapper;
use OCP\DB\Exception;
use OCP\IDBConnection;
use function is_int;

/**
 * @template-extends QBMapper<Note>
 */
class NoteMapper extends QBMapper {
	public const DB_NAME = 'login_notes';

	public function __construct(IDBConnection $db) {
		parent::__construct($db, self::DB_NAME, Note::class);
	}

	/**
	 * @param int $id
	 * @return Note
	 * @throws DoesNotExistException
	 * @throws MultipleObjectsReturnedException|Exception
	 */
	public function getById(int $id): Note {
		$query = $this->db->getQueryBuilder();

		$query->select('*')
			->from($this->getTableName())
			->where(
				$query->expr()->eq('id', $query->createNamedParameter($id))
			);

		return $this->findEntity($query);
	}

	/**
	 * @param int|null $limit
	 * @param int|null $offset
	 * @return Note[]
	 * @psalm-return Note[]
	 * @throws Exception
	 */
	public function getNotes(?int $limit = null, ?int $offset = null): array {
		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from(self::DB_NAME)
			->orderBy('created_at', 'DESC')
		;
		if (is_int($limit)) {
			$query->setMaxResults($limit);
		}
		if (is_int($offset)) {
			$query->setFirstResult($offset);
		}

		return $this->findEntities($query);
	}
}
